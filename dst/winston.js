"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const moment = require("moment");
const { combine, printf } = winston_1.format;
/**
 * winston logger configuration
 * levels: fatal, error, warning, info, debug
 */
const logger = winston_1.createLogger({
    format: combine(printf(info => {
        if (info && info.level === 'warn') {
            return `[${moment().format()}][WARNING]${info.message}`;
        }
        else {
            return `[${moment().format()}][${info.level.toUpperCase()}]${info.message}`;
        }
    })),
    transports: [
        new (require('winston-daily-rotate-file'))({
            timestamp: () => `[${moment().format()}]`,
            datePattern: 'YYYYMMDD',
            filename: `winston_%DATE%.log`,
            dirname: 'logs',
        })
    ],
    exitOnError: false,
});
exports.default = logger;
//# sourceMappingURL=winston.js.map
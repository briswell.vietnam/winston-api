"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * sequelize
 */
const domain_1 = require("@winston/domain");
const createDebug = require("debug");
const debug = createDebug('api:sequelize');
const sequelize = new domain_1.Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USERNAME, process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    benchmark: true,
    timezone: '+09:00',
    port: parseInt(process.env.MYSQL_PORT, 10),
    logging: (logStr, time) => {
        debug(`${logStr} (took ${time}ms)`);
    }
});
sequelize.authenticate().then(() => {
    debug('MySQL server connected');
}).catch((err) => {
    debug(`MySQL connection error ${err}`);
    process.exit();
});
exports.default = sequelize;
//# sourceMappingURL=sequelize.js.map
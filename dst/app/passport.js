"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * PASSPORT MIDDLEWARE
 */
// tslint:disable: no-magic-numbers
const domain = require("@winston/domain");
const passport = require("passport");
const passport_jwt_1 = require("passport-jwt");
const sequelize_1 = require("./sequelize");
const opts = {
    secretOrKey: process.env.JWT_SECRET || 'briswell-secret-key',
    jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderWithScheme('jwt')
};
exports.strategy = new passport_jwt_1.Strategy(opts, (jwtPayload, done) => __awaiter(this, void 0, void 0, function* () {
    done(null, jwtPayload);
}));
exports.jwtAuthenticate = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => __awaiter(this, void 0, void 0, function* () {
        if (err || info) {
            if (info.name === 'JsonWebTokenError') {
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[401]));
            }
            else if (info.name === 'TokenExpiredError') {
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[402]));
            }
            next(new domain.factory.errors.Unauthorized());
        }
        else {
            const userRepo = new domain.repository.User(sequelize_1.default);
            const result = yield userRepo.userSearchId(user.id);
            // 管理者権限 / 一般権限
            if (result) {
                req.user = user;
                next();
            }
            else {
                // deleted user
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[408]));
            }
        }
    }))(req, res, next);
};
exports.masterJwtAuthenticate = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => __awaiter(this, void 0, void 0, function* () {
        if (err || info) {
            if (info.name === 'JsonWebTokenError') {
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[401]));
            }
            else if (info.name === 'TokenExpiredError') {
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[402]));
            }
            next(new domain.factory.errors.Unauthorized());
        }
        else {
            const userRepo = new domain.repository.User(sequelize_1.default);
            const result = yield userRepo.userSearchId(user.id);
            // 管理者権限
            if (req.user.role === domain.factory.user.role.システム管理者 &&
                result !== null && result.role === domain.factory.user.role.システム管理者) {
                req.user = user;
                next();
            }
            else {
                // 一般権限 / deleted user
                next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[408]));
            }
        }
    }))(req, res, next);
};
//# sourceMappingURL=passport.js.map
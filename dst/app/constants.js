"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants declaration
 */
const domain_1 = require("@winston/domain");
exports.messages = domain_1.factory.message;
exports.status = domain_1.factory.constants.apiStatus.status;
//# sourceMappingURL=constants.js.map
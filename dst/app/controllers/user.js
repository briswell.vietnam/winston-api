"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * ユーザーコントローラー
 */
// tslint:disable:no-magic-numbers
const domain = require("@winston/domain");
// import * as crypto from 'crypto';
const createDebug = require("debug");
const http_status_1 = require("http-status");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const constants_1 = require("../constants");
const sequelize_1 = require("../sequelize");
const api_1 = require("../utils/api");
const winston_1 = require("../../winston");
const jwtSecret = process.env.JWT_SECRET || 'briswell-secret-key';
const expiresIn = process.env.JWT_EXPIRATION || '300s'; // デフォルトは５分
const valueConst = domain.factory.constants.valueConst;
const apiMessage = domain.factory.constants.message;
const debug = createDebug('api:controller:user');
const signToken = (data, refreshToken) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        jwt.sign({
            id: data.id,
            loginId: data.loginId,
            userName: data.userName,
            role: data.role
        }, jwtSecret, { expiresIn }, (err, token) => __awaiter(this, void 0, void 0, function* () {
            if (err) {
                reject(err);
            }
            else {
                const result = {
                    accessToken: token,
                    refreshToken
                };
                if (refreshToken === undefined) {
                    const refreshTokenRepo = new domain.repository.RefreshToken(sequelize_1.default);
                    const newRefreshToken = uuid();
                    yield refreshTokenRepo.refreshTokenModel.create({
                        userId: data.id,
                        refreshToken: newRefreshToken,
                        createdBy: data.id,
                        updatedBy: data.id
                    });
                    result.refreshToken = newRefreshToken;
                }
                resolve(result);
            }
        }));
    }));
});
/**
 * 指定されたログインIDとパスワードの情報を認証する
 */
exports.login = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const hashSecret = process.env.HASH_SECRET || 'hash_secret';
        const userRepo = new domain.repository.User(sequelize_1.default, hashSecret);
        const user = yield userRepo.login({
            loginId: req.body.loginId,
            pass: req.body.pass
        });
        let statusApi = constants_1.status[602];
        let messageApi = apiMessage.ECL602;
        if (user != null && user.pass != null) {
            if (user.disableFlag || user.disableFlag === 1) {
                statusApi = constants_1.status[407];
                messageApi = apiMessage.ECL407;
            }
            else {
                // success
                const token = yield signToken({
                    id: user.id,
                    loginId: user.loginId,
                    userName: user.userName,
                    role: user.role
                });
                res.json({
                    status: constants_1.status[200],
                    message: apiMessage.ECL200,
                    data: {
                        token,
                        user: {
                            id: user.id,
                            loginId: user.loginId,
                            userName: user.userName,
                            role: user.role
                        }
                    }
                });
                return;
            }
        }
        else {
            if (user != null) {
                if (Number(user.failedTime) >= valueConst.maxFailedTime) {
                    statusApi = constants_1.status[404];
                    messageApi = apiMessage.ECL404;
                }
            }
        }
        res.json({
            status: statusApi,
            message: messageApi
        });
        return;
    }
    catch (err) {
        debug(err.message);
        next(err);
    }
});
exports.logout = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const refreshTokenRepo = new domain.repository.RefreshToken(sequelize_1.default);
        const refreshToken = req.body.refreshToken;
        const result = yield refreshTokenRepo.refreshTokenModel.destroy({
            where: { refreshToken },
            individualHooks: true
        });
        if (result >= 1) {
            res.json({ status: constants_1.status[200] });
        }
        else {
            throw new domain.factory.errors.NotFound('refreshToken');
        }
    }
    catch (err) {
        debug(err.message);
        next(err);
    }
});
exports.refresh = (req, res, _) => __awaiter(this, void 0, void 0, function* () {
    try {
        const refreshTokenRepo = new domain.repository.RefreshToken(sequelize_1.default);
        const maxAge = parseInt(process.env.REFRESH_TOKEN_MAX_AGE || '10080', 10); // デフォルトは７日 (7*24*60=10080)
        const checkTokenResult = yield refreshTokenRepo.checkToken({
            token: req.body.refreshToken,
            maxAge
        });
        if (checkTokenResult === undefined) {
            // 406
            res.status(http_status_1.UNAUTHORIZED).json(api_1.throwError(constants_1.status[406]));
        }
        else if (checkTokenResult.id === null) {
            // 804
            res.status(http_status_1.UNAUTHORIZED).json(api_1.throwError(constants_1.status[804]));
        }
        else {
            const token = yield signToken(checkTokenResult, checkTokenResult.refreshToken);
            res.json({
                status: constants_1.status[200],
                message: apiMessage.ECL200,
                token
            });
        }
    }
    catch (err) {
        debug(err.message);
        res.status(http_status_1.UNAUTHORIZED).json(api_1.throwError(constants_1.status[804]));
    }
});
exports.userSearchId = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const userRepo = new domain.repository.User(sequelize_1.default);
        const result = yield userRepo.userSearchId(req.params.id);
        if (result === null) {
            winston_1.default.warn('Call userSearchId api: Data not found');
            res.json('Data not found');
        }
        else {
            winston_1.default.info('Call userSearchId api successfully!');
            res.json(api_1.wrapResult([result]));
        }
    }
    catch (err) {
        winston_1.default.error(err.message);
        debug(err.message);
        next(err);
    }
});
//# sourceMappingURL=user.js.map
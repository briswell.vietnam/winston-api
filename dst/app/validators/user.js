"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * ユーザーバリデーション
 */
// tslint:disable:no-submodule-imports
// tslint:disable:no-magic-numbers
const check_1 = require("express-validator/check");
const filter_1 = require("express-validator/filter");
const constants_1 = require("../constants");
const customSanitizer_1 = require("./customSanitizer");
exports.login = [
    check_1.body('loginId')
        .exists({ checkNull: true }).withMessage(constants_1.messages.createEssential('メールアドレス')),
    check_1.body('pass')
        .exists({ checkNull: true }).withMessage(constants_1.messages.createEssential('パスワード'))
];
exports.refreshToken = [
    filter_1.sanitizeBody('*').customSanitizer(customSanitizer_1.emptyStringAsNull),
    check_1.body('refreshToken')
        .exists({ checkNull: true }).withMessage(constants_1.messages.createEssential('refreshToken'))
        .isUUID().withMessage(constants_1.messages.effectivenessError('refreshToken'))
];
exports.logout = exports.refreshToken;
//# sourceMappingURL=user.js.map
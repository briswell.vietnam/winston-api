"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
/**
 * date validator
 * @param value date to be tested
 */
exports.isValidDate = (value) => __awaiter(this, void 0, void 0, function* () {
    const date = moment(value, [
        'YYYY-MM-DD',
        'YYYY-MM-DD HH:mm',
        'YYYY-MM-DD HH:mm:ss',
        'YYYY/MM/DD',
        'YYYY/MM/DD HH:mm:ss',
        'YYYY/MM/DD HH:mm',
        'YYYY/M/DD',
        'YYYY/M/DD HH:mm',
        'YYYY/M/DD HH:mm:ss'
    ], true);
    if (!date.isValid()) {
        return false;
    }
    else {
        return true;
    }
});
exports.isValidEmail = (value) => __awaiter(this, void 0, void 0, function* () {
    // tslint:disable-next-line: max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (value === '') {
        return true;
    }
    return re.test(String(value).toLowerCase());
});
exports.checkFlag = (_, option) => __awaiter(this, void 0, void 0, function* () {
    const defaultVal = '0.1';
    if (option.req.query[option.path] instanceof Array) {
        const length = option.req.query[option.path].length;
        let checkLen = 0;
        for (let index = 0; index < length; index++) {
            const element = option.req.query[option.path][index];
            option.req.query[option.path][index] = !isNaN(element) ? element : defaultVal;
            if (element.length === 0) {
                checkLen++;
            }
        }
        // delete when all value of array is empty
        if (checkLen === length) {
            delete option.req.query[option.path];
        }
    }
    else if (option.req.query[option.path] !== undefined) {
        const value = option.req.query[option.path];
        if (value) {
            option.req.query[option.path] = !isNaN(value) ? value : defaultVal;
        }
    }
    return true;
});
exports.checkCharOneByte = (value) => __awaiter(this, void 0, void 0, function* () {
    const re = /^[a-zA-Z0-9!-/:-@\\[-`{-~ ]+$/;
    if (value === '') {
        return true;
    }
    return re.test(String(value).toLowerCase());
});
exports.checkRegexCharByte = (value) => __awaiter(this, void 0, void 0, function* () {
    const re = /^[a-zA-Z0-9#\$\%()\*\+\-\.\/:;?@[\]_{}~]+$/;
    if (value === '') {
        return true;
    }
    return re.test(String(value).toLowerCase());
});
//# sourceMappingURL=customValidator.js.map
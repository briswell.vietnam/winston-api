"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * numberSanitizer
 * @param value string
 */
exports.numberSanitizer = (value) => {
    if (value.length === 0 || isNaN(value)) {
        return null;
    }
    else {
        return parseInt(value, 10);
    }
};
/**
 * return null if string is empty
 * @param value string
 */
exports.emptyStringAsNull = (value) => value.length > 0 ? value : null;
exports.removeField = (_, option) => {
    delete option.req.body[option.path];
};
//# sourceMappingURL=customSanitizer.js.map
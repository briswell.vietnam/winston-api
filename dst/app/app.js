"use strict";
/**
 * expressアプリケーション
 */
const bodyParser = require("body-parser");
const cors = require("cors");
// import * as createDebug from 'debug';
const express = require("express");
const helmet = require("helmet");
const passport = require("passport");
const qs = require("qs");
const checkEnv_1 = require("./checkEnv");
const errorHandler_1 = require("./middlewares/errorHandler");
const notFoundHandler_1 = require("./middlewares/notFoundHandler");
const passport_1 = require("./passport");
const routes_1 = require("./routes");
const auth_1 = require("./routes/auth");
// const debug = createDebug('chevre-api:app');
checkEnv_1.default();
const app = express();
app.use(passport.initialize());
app.set('query parser', (str) => qs.parse(str, {
    arrayLimit: 1000,
    parseArrays: true,
    allowDots: false,
    allowPrototypes: true
}));
const options = {
    origin: '*',
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token', 'Authorization'],
    credentials: false,
    methods: ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST', 'DELETE'],
    preflightContinue: false,
    optionsSuccessStatus: 204
};
app.use(cors(options));
app.use(helmet());
app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ['\'self\'']
        // styleSrc: ['\'unsafe-inline\'']
    }
}));
app.use(helmet.referrerPolicy({ policy: 'no-referrer' })); // 型定義が非対応のためany
const SIXTY_DAYS_IN_SECONDS = 5184000;
app.use(helmet.hsts({
    maxAge: SIXTY_DAYS_IN_SECONDS,
    includeSubdomains: false
}));
// api version
// tslint:disable-next-line:no-require-imports no-var-requires
const packageInfo = require('../../package.json');
app.use((__, res, next) => {
    res.setHeader('x-api-verion', packageInfo.version);
    next();
});
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ limit: '1mb', extended: true }));
app.use('/v1/', auth_1.default);
passport.use(passport_1.strategy);
// routers
app.use('/v1/', passport_1.jwtAuthenticate, routes_1.router);
app.use('/v1/', passport_1.masterJwtAuthenticate, routes_1.masterRouter);
app.use('/check/', (_, res) => {
    res.send('OK');
});
// 404
app.use(notFoundHandler_1.default);
// error handlers
app.use(errorHandler_1.default);
module.exports = app;
//# sourceMappingURL=app.js.map
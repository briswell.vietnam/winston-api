"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * バリデーターミドルウェア
 * リクエストのパラメータ(query strings or body parameters)に対するバリデーション
 */
// tslint:disable:no-magic-numbers
const domain = require("@winston/domain");
const createDebug = require("debug");
// tslint:disable-next-line:no-submodule-imports
const check_1 = require("express-validator/check");
// import { BAD_REQUEST } from 'http-status';
const constants_1 = require("../constants");
// import { APIError } from '../error/api';
const debug = createDebug('api:middlewares:validator');
exports.default = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const validatorResult = check_1.validationResult(req);
    if (!validatorResult.isEmpty()) {
        const errors = validatorResult.array().map((mappedRrror) => {
            return new domain.factory.errors.Argument(mappedRrror.param, mappedRrror.msg);
        });
        debug('validation result not empty...', errors);
        const requiredValidations = errors.filter((err) => err.message.includes(constants_1.messages.createEssential('')));
        // 必須入力チェック
        if (requiredValidations && requiredValidations.length > 0) {
            res.json({
                status: constants_1.status[601],
                message: constants_1.messages.ECL601,
                data: []
            });
        }
        else {
            // サイズチェック || フォーマットチェック
            res.json({
                status: constants_1.status[600],
                message: constants_1.messages.ECL600,
                data: []
            });
        }
    }
    else {
        next();
    }
});
//# sourceMappingURL=validator.js.map
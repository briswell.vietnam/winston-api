"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Buffer取得ミドルウェア
 */
const charset = require("encoding-japanese");
const fileType = require("file-type");
// tslint:disable-next-line: no-implicit-dependencies
const iconvlite = require("iconv-lite");
exports.default = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    if (!req.is('application/octet-stream')) {
        next();
    }
    else {
        const data = [];
        req.on('data', (chunk) => {
            data.push(chunk);
        });
        req.on('end', () => {
            if (data.length <= 0) {
                next();
            }
            else {
                const buffer = Buffer.concat(data);
                const detectMatch = charset.detect(buffer);
                const ext = fileType(buffer);
                if (detectMatch !== null && ext === null) {
                    try {
                        res.locals.ext = 'csv';
                        res.locals.file = iconvlite.decode(buffer, detectMatch);
                    }
                    catch (error) {
                        res.locals.ext = '';
                        res.locals.file = buffer;
                    }
                }
                else {
                    res.locals.ext = ext;
                    res.locals.file = buffer;
                }
                next();
            }
        });
    }
});
//# sourceMappingURL=stream2Buffer.js.map
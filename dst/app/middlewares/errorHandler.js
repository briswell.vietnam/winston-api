"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * エラーハンドラーミドルウェア
 */
// tslint:disable:no-magic-numbers
const domain = require("@winston/domain");
const createDebug = require("debug");
const constants_1 = require("../constants");
const api_1 = require("../utils/api");
const http_status_1 = require("http-status");
const api_2 = require("../error/api");
const debug = createDebug('api:middlewares:errorHandler');
exports.default = (err, __, res, next) => {
    debug(err.message);
    if (res.headersSent) {
        next(err);
        return;
    }
    let apiError;
    if (err instanceof api_2.APIError) {
        apiError = err;
    }
    else {
        // エラー配列が入ってくることもある
        if (Array.isArray(err)) {
            apiError = new api_2.APIError(domainError2httpStatusCode(err[0]), err);
        }
        else if (err instanceof domain.factory.errors.Error) {
            apiError = new api_2.APIError(domainError2httpStatusCode(err), [err]);
        }
        else {
            // 500
            apiError = new api_2.APIError(http_status_1.INTERNAL_SERVER_ERROR, [new domain.factory.errors.Error('InternalServerError', err.message)]);
        }
    }
    // Unauthorized error
    if (apiError.code === http_status_1.UNAUTHORIZED) {
        const unauthorizedErr = apiError.errors[0];
        // JsonWebTokenError || TokenExpiredError
        if (unauthorizedErr.apiStatus === constants_1.status[401] ||
            unauthorizedErr.apiStatus === constants_1.status[402] ||
            unauthorizedErr.apiStatus === constants_1.status[408]) {
            res.status(apiError.code).json(api_1.throwError(unauthorizedErr.apiStatus));
            return;
        }
    }
    // not found URL
    if (apiError.code === http_status_1.NOT_FOUND) {
        res.status(apiError.code).json({
            status: 'Not Found',
            message: 'Not Found URL'
        });
        return;
    }
    res.status(apiError.code).json({
        status: 'error',
        dataCount: 0,
        data: [],
        error: apiError.toObject()
    });
};
/**
 * domainエラーをHTTPステータスコードへ変換する
 * @param err domainエラー
 */
function domainError2httpStatusCode(err) {
    let statusCode = http_status_1.BAD_REQUEST;
    switch (true) {
        // 401
        case (err instanceof domain.factory.errors.Unauthorized):
            statusCode = http_status_1.UNAUTHORIZED;
            break;
        // 403
        case (err instanceof domain.factory.errors.Forbidden):
            statusCode = http_status_1.FORBIDDEN;
            break;
        // 404
        case (err instanceof domain.factory.errors.NotFound):
            statusCode = http_status_1.NOT_FOUND;
            break;
        // 409
        case (err instanceof domain.factory.errors.AlreadyInUse):
            statusCode = http_status_1.CONFLICT;
            break;
        // 429
        case (err instanceof domain.factory.errors.RateLimitExceeded):
            statusCode = http_status_1.TOO_MANY_REQUESTS;
            break;
        // 502
        case (err instanceof domain.factory.errors.NotImplemented):
            statusCode = http_status_1.NOT_IMPLEMENTED;
            break;
        // 503
        case (err instanceof domain.factory.errors.ServiceUnavailable):
            statusCode = http_status_1.SERVICE_UNAVAILABLE;
            break;
        // 400
        default:
            statusCode = http_status_1.BAD_REQUEST;
    }
    return statusCode;
}
//# sourceMappingURL=errorHandler.js.map
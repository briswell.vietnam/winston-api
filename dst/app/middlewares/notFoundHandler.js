"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 404ハンドラーミドルウェア
 */
const domain = require("@winston/domain");
exports.default = (req, __, next) => {
    next(new domain.factory.errors.NotFound(`router for [${req.originalUrl}]`));
};
//# sourceMappingURL=notFoundHandler.js.map
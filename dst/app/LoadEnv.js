"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line: missing-jsdoc
const commandLineArgs = require("command-line-args");
const dotenv = require("dotenv");
// Setup command line options
const options = commandLineArgs([
    {
        name: 'env',
        alias: 'e',
        defaultValue: 'development',
        type: String
    }
]);
// Set the env file
const result = dotenv.config({
    path: `./env/${options.env}.env`
});
if (result.error) {
    throw result.error;
}
//# sourceMappingURL=LoadEnv.js.map
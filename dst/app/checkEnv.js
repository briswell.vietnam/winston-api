"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * check for required env variable
 */
const createDebug = require("debug");
const debug = createDebug('api:evn-checker');
const runCheck = () => {
    let checkOk = true;
    const envList = [
        'MYSQL_DATABASE',
        'MYSQL_USERNAME',
        'MYSQL_PASSWORD',
        'MYSQL_HOST',
        'MYSQL_PORT'
    ];
    envList.forEach((env) => {
        if (process.env[env] === undefined) {
            debug(`The env variable "${env}" is required!`);
            checkOk = false;
        }
    });
    if (!checkOk) {
        debug(`API terminating`);
        process.exit();
    }
};
exports.default = runCheck;
//# sourceMappingURL=checkEnv.js.map
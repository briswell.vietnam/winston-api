"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * ユーザールター
 */
const express_1 = require("express");
const userController = require("../controllers/user");
const validator_1 = require("../middlewares/validator");
const userValidator = require("../validators/user");
const userRoute = express_1.Router();
userRoute.post('/userLogout', userValidator.logout, validator_1.default, userController.logout);
userRoute.get('/:id([0-9]+)', userController.userSearchId);
exports.default = userRoute;
//# sourceMappingURL=user.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 認証ルター
 */
const express_1 = require("express");
const userController = require("../controllers/user");
const validator_1 = require("../middlewares/validator");
const userValidator = require("../validators/user");
const authRoute = express_1.Router();
authRoute.post('/userLogin', userValidator.login, validator_1.default, userController.login);
authRoute.post('/user/refreshToken', userValidator.refreshToken, validator_1.default, userController.refresh);
exports.default = authRoute;
//# sourceMappingURL=auth.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * ルーター
 */
const express = require("express");
const user_1 = require("./user");
exports.masterRouter = express.Router();
exports.masterRouter.use('/user', user_1.default);
exports.router = express.Router();
//# sourceMappingURL=index.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * API utilities
 */
// tslint:disable:no-magic-numbers
const constants_1 = require("../constants");
function wrapResult(result, count) {
    if (count !== undefined) {
        if (count === 0) {
            return {
                status: constants_1.status[603],
                message: constants_1.messages.ECL603,
                data: [],
                dataCount: ''
            };
        }
        else {
            const response = {
                status: constants_1.status[200],
                message: constants_1.messages.ECL200,
                data: result,
                dataCount: count
            };
            if (result.length === 0) {
                delete response.data;
            }
            return response;
        }
    }
    else {
        return {
            status: constants_1.status[200],
            message: constants_1.messages.ECL200,
            data: result
        };
    }
}
exports.wrapResult = wrapResult;
function throwError(st, params) {
    return {
        status: constants_1.status[st],
        message: (params === undefined) ? constants_1.messages[`ECL${st}`] : String(constants_1.messages[`ECL${st}`]).replace('{0}', params)
    };
}
exports.throwError = throwError;
//# sourceMappingURL=api.js.map
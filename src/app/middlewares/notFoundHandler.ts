/**
 * 404ハンドラーミドルウェア
 */
import * as domain from '@winston/domain';
import { NextFunction, Request, Response } from 'express';

export default (req: Request, __: Response, next: NextFunction) => {
    next(new domain.factory.errors.NotFound(`router for [${req.originalUrl}]`));
};

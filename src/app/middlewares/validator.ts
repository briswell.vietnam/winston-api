/**
 * バリデーターミドルウェア
 * リクエストのパラメータ(query strings or body parameters)に対するバリデーション
 */
// tslint:disable:no-magic-numbers
import * as domain from '@winston/domain';
import * as createDebug from 'debug';
import { NextFunction, Request, Response } from 'express';
// tslint:disable-next-line:no-submodule-imports
import { validationResult } from 'express-validator/check';
// import { BAD_REQUEST } from 'http-status';
import { messages, status } from '../constants';
// import { APIError } from '../error/api';

const debug = createDebug('api:middlewares:validator');

export default async (req: Request, res: Response, next: NextFunction) => {
    const validatorResult = validationResult<{ param: string; msg: string }>(req);
    if (!validatorResult.isEmpty()) {
        const errors = validatorResult.array().map((mappedRrror) => {
            return new domain.factory.errors.Argument(mappedRrror.param, mappedRrror.msg);
        });

        debug('validation result not empty...', errors);

        const requiredValidations = errors.filter((err) => err.message.includes(messages.createEssential('')));

        // 必須入力チェック
        if (requiredValidations && requiredValidations.length > 0) {
            res.json({
                status: status[601],
                message: messages.ECL601,
                data: []
            });
        } else {
            // サイズチェック || フォーマットチェック
            res.json({
                status: status[600],
                message: messages.ECL600,
                data: []
            });
        }
    } else {
        next();
    }
};

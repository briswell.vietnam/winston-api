/**
 * エラーハンドラーミドルウェア
 */
// tslint:disable:no-magic-numbers
import * as domain from '@winston/domain';
import * as createDebug from 'debug';
import { NextFunction, Request, Response } from 'express';
import { status } from '../constants';
import { throwError } from '../utils/api';

import {
  BAD_REQUEST,
  CONFLICT, FORBIDDEN,
  INTERNAL_SERVER_ERROR,
  NOT_FOUND,
  NOT_IMPLEMENTED,
  SERVICE_UNAVAILABLE,
  TOO_MANY_REQUESTS,
  UNAUTHORIZED
} from 'http-status';

import { APIError } from '../error/api';

type statusCodeType = typeof BAD_REQUEST |
                      typeof FORBIDDEN |
                      typeof UNAUTHORIZED |
                      typeof NOT_FOUND |
                      typeof CONFLICT |
                      typeof TOO_MANY_REQUESTS |
                      typeof NOT_IMPLEMENTED |
                      typeof SERVICE_UNAVAILABLE;

const debug = createDebug('api:middlewares:errorHandler');

export default (err: any, __: Request, res: Response, next: NextFunction) => {
  debug(err.message);
  if (res.headersSent) {
    next(err);

    return;
  }

  let apiError: APIError;
  if (err instanceof APIError) {
    apiError = err;
  } else {
    // エラー配列が入ってくることもある
    if (Array.isArray(err)) {
      apiError = new APIError(domainError2httpStatusCode(err[0]), err);
    } else if (err instanceof domain.factory.errors.Error) {
      apiError = new APIError(domainError2httpStatusCode(err), [err]);
    } else {
      // 500
      apiError = new APIError(INTERNAL_SERVER_ERROR, [new domain.factory.errors.Error(<any>'InternalServerError', err.message)]);
    }
  }

  // Unauthorized error
  if (apiError.code === UNAUTHORIZED) {
    const unauthorizedErr = apiError.errors[0];

    // JsonWebTokenError || TokenExpiredError
    if (unauthorizedErr.apiStatus === status[401] ||
      unauthorizedErr.apiStatus === status[402] ||
      unauthorizedErr.apiStatus === status[408]) {
      res.status(apiError.code).json(throwError(unauthorizedErr.apiStatus));

      return;
    }
  }

  // not found URL
  if (apiError.code === NOT_FOUND) {
    res.status(apiError.code).json({
      status: 'Not Found',
      message: 'Not Found URL'
    });

    return;
  }

  res.status(apiError.code).json({
    status: 'error',
    dataCount: 0,
    data: [],
    error: apiError.toObject()
  });

};

/**
 * domainエラーをHTTPステータスコードへ変換する
 * @param err domainエラー
 */
function domainError2httpStatusCode(err: domain.factory.errors.Error) {
  let statusCode: statusCodeType = BAD_REQUEST;

  switch (true) {
    // 401
    case (err instanceof domain.factory.errors.Unauthorized):
      statusCode = UNAUTHORIZED;
      break;

    // 403
    case (err instanceof domain.factory.errors.Forbidden):
      statusCode = FORBIDDEN;
      break;

    // 404
    case (err instanceof domain.factory.errors.NotFound):
      statusCode = NOT_FOUND;
      break;

    // 409
    case (err instanceof domain.factory.errors.AlreadyInUse):
      statusCode = CONFLICT;
      break;

    // 429
    case (err instanceof domain.factory.errors.RateLimitExceeded):
      statusCode = TOO_MANY_REQUESTS;
      break;

    // 502
    case (err instanceof domain.factory.errors.NotImplemented):
      statusCode = NOT_IMPLEMENTED;
      break;

    // 503
    case (err instanceof domain.factory.errors.ServiceUnavailable):
      statusCode = SERVICE_UNAVAILABLE;
      break;

    // 400
    default:
      statusCode = BAD_REQUEST;
  }

  return statusCode;
}

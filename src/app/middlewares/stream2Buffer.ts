/**
 * Buffer取得ミドルウェア
 */
import * as charset from 'encoding-japanese';
import { NextFunction, Request, Response } from 'express';
import * as fileType from 'file-type';
// tslint:disable-next-line: no-implicit-dependencies
import * as iconvlite from 'iconv-lite';

export default async (req: Request, res: Response, next: NextFunction) => {
  if (!req.is('application/octet-stream')) {
    next();
  } else {
    const data: any[] = [];
    req.on('data', (chunk) => {
        data.push(chunk);
    });
    req.on('end', () => {
      if (data.length <= 0) {
        next();
      } else {
        const buffer = Buffer.concat(data);
        const detectMatch = charset.detect(buffer);
        const ext = fileType(buffer);

        if (detectMatch !== null && ext === null) {
          try {
            res.locals.ext = 'csv';
            res.locals.file = iconvlite.decode(buffer, detectMatch);
          } catch (error) {
            res.locals.ext = '';
            res.locals.file = buffer;
          }
        } else {
          res.locals.ext = ext;
          res.locals.file = buffer;
        }
        next();
      }
    });
  }
};

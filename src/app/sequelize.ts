/**
 * sequelize
 */
import { Sequelize } from '@winston/domain';
import * as createDebug from 'debug';

const debug = createDebug('api:sequelize');

const sequelize = new Sequelize(
  <string>process.env.MYSQL_DATABASE,
  <string>process.env.MYSQL_USERNAME,
  <string>process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    benchmark: true,
    timezone: '+09:00',
    port: parseInt(<string>process.env.MYSQL_PORT, 10),
    logging: (logStr: string, time: number) => {
      debug(`${logStr} (took ${time}ms)`);
    }
  }
);

sequelize.authenticate().then(() => {
  debug('MySQL server connected');
}).catch((err: any) => {
  debug(`MySQL connection error ${err}`);
  process.exit();
});

export default sequelize;

/**
 * API utilities
 */
// tslint:disable:no-magic-numbers
import { messages, status } from '../constants';

export function wrapResult(result: any[], count?: number) {
  if (count !== undefined) {
    if (count === 0) {
      return {
        status: status[603],
        message: messages.ECL603,
        data: [],
        dataCount: ''
      };
    } else {
      const response = {
        status: status[200],
        message: messages.ECL200,
        data: result,
        dataCount: count
      };

      if (result.length === 0) {
        delete response.data;
      }

      return response;
    }
  } else {
    return {
      status: status[200],
      message: messages.ECL200,
      data: result
    };
  }
}

export function throwError(st: any, params?: any) {
  return {
    status: (<any>status)[st],
    message: (params === undefined) ? (<any>messages)[`ECL${st}`] : String((<any>messages)[`ECL${st}`]).replace('{0}', params)
  };
}

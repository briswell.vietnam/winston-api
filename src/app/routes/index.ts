/**
 * ルーター
 */
import * as express from 'express';
import userRoute from './user';

export const masterRouter = express.Router();

masterRouter.use('/user', userRoute);

export const router = express.Router();

/**
 * 認証ルター
 */
import { Router } from 'express';
import * as userController from '../controllers/user';
import validators from '../middlewares/validator';
import * as userValidator from '../validators/user';

const authRoute = Router();

authRoute.post(
  '/userLogin',
  userValidator.login,
  validators,
  userController.login
);

authRoute.post(
  '/user/refreshToken',
  userValidator.refreshToken,
  validators,
  userController.refresh
);

export default authRoute;

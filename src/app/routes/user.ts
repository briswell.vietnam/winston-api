/**
 * ユーザールター
 */
import { Router } from 'express';
import * as userController from '../controllers/user';
import validators from '../middlewares/validator';
import * as userValidator from '../validators/user';

const userRoute = Router();

userRoute.post(
  '/userLogout',
  userValidator.logout,
  validators,
  userController.logout
);

userRoute.get('/:id([0-9]+)', userController.userSearchId);

export default userRoute;

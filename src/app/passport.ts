/**
 * PASSPORT MIDDLEWARE
 */
// tslint:disable: no-magic-numbers
import * as domain from '@winston/domain';
import { NextFunction, Request, Response } from 'express';
import * as passport from 'passport';
import { ExtractJwt, Strategy as JWTStrategy, StrategyOptions } from 'passport-jwt';
import sequelize from './sequelize';

const opts: StrategyOptions = {
  secretOrKey: process.env.JWT_SECRET || 'briswell-secret-key',
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
};

export const strategy = new JWTStrategy(opts, async (jwtPayload, done) => {
  done(null, jwtPayload);
});

export const jwtAuthenticate = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', { session: false }, async (err, user, info) => {

    if (err || info) {
      if (info.name === 'JsonWebTokenError') {
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[401]));
      } else if (info.name === 'TokenExpiredError') {
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[402]));
      }
      next(new domain.factory.errors.Unauthorized());
    } else {
      const userRepo = new domain.repository.User(sequelize);
      const result = await userRepo.userSearchId(user.id);

      // 管理者権限 / 一般権限
      if (result) {
        req.user = user;
        next();
      } else {
        // deleted user
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[408]));
      }
    }
  })(req, res, next);
};

export const masterJwtAuthenticate = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', { session: false }, async (err, user, info) => {

    if (err || info) {
      if (info.name === 'JsonWebTokenError') {
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[401]));
      } else if (info.name === 'TokenExpiredError') {
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[402]));
      }
      next(new domain.factory.errors.Unauthorized());
    } else {
      const userRepo = new domain.repository.User(sequelize);
      const result = await userRepo.userSearchId(user.id);

      // 管理者権限
      if (req.user.role === domain.factory.user.role.システム管理者 &&
          result !== null && result.role === domain.factory.user.role.システム管理者) {
        req.user = user;
        next();
      } else {
        // 一般権限 / deleted user
        next(new domain.factory.errors.Unauthorized('', domain.factory.constants.apiStatus.status[408]));
      }
    }
  })(req, res, next);
};

/**
 * Constants declaration
 */
import { factory } from '@winston/domain';

export const messages = factory.message;
export const status = factory.constants.apiStatus.status;

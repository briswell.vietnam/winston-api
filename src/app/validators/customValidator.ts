// tslint:disable: no-increment-decrement
import { Request } from 'express';
import * as moment from 'moment';

/**
 * date validator
 * @param value date to be tested
 */
export const isValidDate = async (value: string) => {
  const date = moment(
    value, [
      'YYYY-MM-DD',
      'YYYY-MM-DD HH:mm',
      'YYYY-MM-DD HH:mm:ss',
      'YYYY/MM/DD',
      'YYYY/MM/DD HH:mm:ss',
      'YYYY/MM/DD HH:mm',
      'YYYY/M/DD',
      'YYYY/M/DD HH:mm',
      'YYYY/M/DD HH:mm:ss'
    ],
    true);

  if (!date.isValid()) {
      return false;
    } else {
      return true;
    }
};

export const isValidEmail = async (value: string) => {
  // tslint:disable-next-line: max-line-length
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (value === '') {
    return true;
  }

  return re.test(String(value).toLowerCase());
};

export const checkFlag = async (_: string, option: { req: Request; path: string }) => {
  const defaultVal = '0.1';

  if (option.req.query[option.path] instanceof Array) {

    const length = option.req.query[option.path].length;
    let checkLen = 0;

    for (let index = 0; index < length; index++) {
      const element = option.req.query[option.path][index];

      option.req.query[option.path][index] = !isNaN(element) ? element : defaultVal;
      if (element.length === 0) {
        checkLen++;
      }
    }

    // delete when all value of array is empty
    if (checkLen === length) {
      delete option.req.query[option.path];
    }

  } else if (option.req.query[option.path] !== undefined) {
    const value = option.req.query[option.path];
    if (value) {
      option.req.query[option.path] = !isNaN(value) ? value : defaultVal;
    }
  }

  return true;
};

export const checkCharOneByte = async (value: string) => {
  const re = /^[a-zA-Z0-9!-/:-@\\[-`{-~ ]+$/;
  if (value === '') {
    return true;
  }

  return re.test(String(value).toLowerCase());
};

export const checkRegexCharByte = async (value: string) => {
  const re = /^[a-zA-Z0-9#\$\%()\*\+\-\.\/:;?@[\]_{}~]+$/;
  if (value === '') {
    return true;
  }

  return re.test(String(value).toLowerCase());
};

/**
 * ユーザーバリデーション
 */
// tslint:disable:no-submodule-imports
// tslint:disable:no-magic-numbers
import { body } from 'express-validator/check';
import { sanitizeBody } from 'express-validator/filter';
import { messages } from '../constants';
import { emptyStringAsNull } from './customSanitizer';

export const login = [
  body('loginId')
    .exists({ checkNull: true }).withMessage(messages.createEssential('メールアドレス')),

  body('pass')
    .exists({ checkNull: true }).withMessage(messages.createEssential('パスワード'))
];

export const refreshToken = [
  sanitizeBody('*').customSanitizer(emptyStringAsNull),

  body('refreshToken')
    .exists({ checkNull: true }).withMessage(messages.createEssential('refreshToken'))
    .isUUID().withMessage(messages.effectivenessError('refreshToken'))
];

export const logout = refreshToken;
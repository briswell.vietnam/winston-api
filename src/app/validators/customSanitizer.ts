import { Request } from 'express';

/**
 * numberSanitizer
 * @param value string
 */
export const numberSanitizer = (value: string) => {
  if (value.length === 0 || isNaN(<any>value)) {
    return null;
  } else {
    return parseInt(value, 10);
  }
};

/**
 * return null if string is empty
 * @param value string
 */
export const emptyStringAsNull = (value: string) => value.length > 0 ? value : null;

export const removeField = (_: string, option: { req: Request; path: string }) => {
  delete option.req.body[option.path];
};

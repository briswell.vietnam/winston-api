/**
 * ユーザーコントローラー
 */
// tslint:disable:no-magic-numbers
import * as domain from '@winston/domain';
// import * as crypto from 'crypto';
import * as createDebug from 'debug';
import { NextFunction, Request, Response } from 'express';
import { UNAUTHORIZED } from 'http-status';
import * as jwt from 'jsonwebtoken';
import * as uuid from 'uuid';
import { status } from '../constants';
import sequelize from '../sequelize';
import { throwError, wrapResult } from '../utils/api';
import winston from '../../winston';

const jwtSecret = process.env.JWT_SECRET || 'briswell-secret-key';
const expiresIn = process.env.JWT_EXPIRATION || '300s'; // デフォルトは５分
const valueConst = domain.factory.constants.valueConst;
const apiMessage = domain.factory.constants.message;
const debug = createDebug('api:controller:user');

const signToken = async (data: { id: number; loginId: string; userName: string; role: number }, refreshToken?: string) => {
  return new Promise(async (resolve, reject) => {
    jwt.sign(
      {
        id: data.id,
        loginId: data.loginId,
        userName: data.userName,
        role: data.role
      },
      jwtSecret,
      { expiresIn },
      async (err, token) => {
        if (err) {
          reject(err);
        } else {
          const result = {
            accessToken: token,
            refreshToken
          };
          if (refreshToken === undefined) {
            const refreshTokenRepo = new domain.repository.RefreshToken(sequelize);
            const newRefreshToken = uuid();
            await refreshTokenRepo.refreshTokenModel.create({
              userId: data.id,
              refreshToken: newRefreshToken,
              createdBy: data.id,
              updatedBy: data.id
            });
            result.refreshToken = newRefreshToken;
          }
          resolve(result);
        }
      }
    );
  });
};

/**
 * 指定されたログインIDとパスワードの情報を認証する
 */
export const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const hashSecret = process.env.HASH_SECRET || 'hash_secret';
    const userRepo = new domain.repository.User(sequelize, hashSecret);

    const user = await userRepo.login({
      loginId: req.body.loginId,
      pass: req.body.pass
    });

    let statusApi = status[602];
    let messageApi = apiMessage.ECL602;

    if (user != null && user.pass != null) {
      if (user.disableFlag || user.disableFlag === 1) {
        statusApi = status[407];
        messageApi = apiMessage.ECL407;
      } else {
        // success
        const token = await signToken(
          {
            id: user.id!,
            loginId: user.loginId,
            userName: user.userName!,
            role: user.role
          });

        res.json({
          status: status[200],
          message: apiMessage.ECL200,
          data: {
            token,
            user: {
              id: user.id,
              loginId: user.loginId,
              userName: user.userName,
              role: user.role
            }
          }
        });

        return;
      }
    } else {
      if (user != null) {
        if (Number(user.failedTime) >= valueConst.maxFailedTime) {
          statusApi = status[404];
          messageApi = apiMessage.ECL404;
        }
      }
    }

    res.json({
      status: statusApi,
      message: messageApi
    });

    return;
  } catch (err) {
    debug(err.message);
    next(err);
  }
};

export const logout = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const refreshTokenRepo = new domain.repository.RefreshToken(sequelize);
    const refreshToken = req.body.refreshToken;
    const result = await refreshTokenRepo.refreshTokenModel.destroy({
      where: {refreshToken},
      individualHooks: true
    });
    if (result >= 1) {
      res.json({ status: status[200] });
    } else {
      throw new domain.factory.errors.NotFound('refreshToken');
    }
  } catch (err) {
    debug(err.message);
    next(err);
  }
};

export const refresh = async (req: Request, res: Response, _: NextFunction) => {
  try {
    const refreshTokenRepo = new domain.repository.RefreshToken(sequelize);
    const maxAge = parseInt(process.env.REFRESH_TOKEN_MAX_AGE || '10080', 10); // デフォルトは７日 (7*24*60=10080)
    const checkTokenResult = await refreshTokenRepo.checkToken({
      token: req.body.refreshToken,
      maxAge
    });
    if (checkTokenResult === undefined) {
      // 406
      res.status(UNAUTHORIZED).json(throwError(status[406]));
    } else if (checkTokenResult.id === null) {
      // 804
      res.status(UNAUTHORIZED).json(throwError(status[804]));
    } else {
      const token = await signToken(checkTokenResult, checkTokenResult.refreshToken);
      res.json({
        status: status[200],
        message: apiMessage.ECL200,
        token
      });
    }
  } catch (err) {
    debug(err.message);
    res.status(UNAUTHORIZED).json(throwError(status[804]));
  }
};

export const userSearchId = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const userRepo = new domain.repository.User(sequelize);
    const result = await userRepo.userSearchId(req.params.id);

    if (result === null) {
      winston.warn('Call userSearchId api: Data not found');
      res.json('Data not found');
    } else {
      winston.info('Call userSearchId api successfully!');
      res.json(wrapResult([result]));
    }
  } catch (err) {
    winston.error(err.message);
    debug(err.message);
    next(err);
  }
};
